//! Raw IO
use register::{mmio::*, register_bitfields};

/// Base of the 3DS SDMMC registers
pub const SDMMC_BASE: *mut RegisterBlock = 0x1000_6000 as *mut RegisterBlock;

register_bitfields! {
    u16,
    SD_CMD [
        CIX OFFSET(0) NUMBITS(5) [],
        CMD OFFSET(5) NUMBITS(2) [
            CMD = 0,
            ACMD = 1,
            UNK0 = 2,
            UNK1 = 3
        ],
        REP OFFSET(8) NUMBITS(3) [
            AUTO = 0,
            NONE = 3,
            BIT48 = 4,
            BUSY_BIT48 = 5,
            BIT_136 = 6,
            BIT48_OCR_NO_CRC = 7
        ],
        NTDT OFFSET(11) NUMBITS(1) [],
        DDIR OFFSET(12) NUMBITS(1) [
            WRITE = 0,
            READ = 1
        ],
        DLEN OFFSET(13) NUMBITS(1) [
            SINGLE = 0,
            MULTIPLE = 1
        ],
        SCMD OFFSET(14) NUMBITS(1) []
    ],
    SD_CARD_PORT_SELECT [
        PORT OFFSET(0) NUMBITS(1) [
            SD = 0,
            EMMC = 1
        ]
    ],
    SD_STOP_INTERNAL_ACTION [
        AUTO_STOP OFFSET(8) NUMBITS(1) [
            ON = 1,
            OFF = 0
        ]
    ],
    SD_IRQ_STATUS0 [
        CMDRESPEND OFFSET(0) NUMBITS(1) [],
        DATAEND OFFSET(2) NUMBITS(1) [],
        CARD_REMOVE OFFSET(3) NUMBITS(1) [],
        CARD_INSERT OFFSET(4) NUMBITS(1) [],
        SIGSTATE OFFSET(5) NUMBITS(1) [],
        WRPROTECT OFFSET(7) NUMBITS(1) [],
        CARD_REMOVE_A OFFSET(8) NUMBITS(1) [],
        CARD_INSERT_A OFFSET(9) NUMBITS(1) [],
        SIGSTATE_A OFFSET(10) NUMBITS(1) []
    ],
    SD_IRQ_STATUS1 [
        CMD_IDX_ERR OFFSET(0) NUMBITS(1) [],
        CRCFAIL OFFSET(1) NUMBITS(1) [],
        STOPBIT_ERR OFFSET(2) NUMBITS(1) [],
        DATATIMEOUT OFFSET(3) NUMBITS(1) [],
        RXOVERFLOW OFFSET(4) NUMBITS(1) [],
        TXUNDERRUN OFFSET(5) NUMBITS(1) [],
        CMDTIMEOUT OFFSET(6) NUMBITS(1) [],
        RXRDY OFFSET(8) NUMBITS(1) [],
        TXRQ OFFSET(9) NUMBITS(1) [],
        CMD_READY OFFSET(13) NUMBITS(1) [],
        CMD_BUSY OFFSET(14) NUMBITS(1) [],
        ILLCMD OFFSET(15) NUMBITS(1) []
    ],
    SD_CARD_CLK_CTL [
        HCLK_DIV OFFSET(0) NUMBITS(8) [],
        SDCLK_PIN_ENABLE OFFSET(8) NUMBITS(1) [],
        SDCLK_FREEZE OFFSET(9) NUMBITS(1) [],
        /// Broken
        HCLK_DIV_DISABLE OFFSET(15) NUMBITS(1) [
            ENABLE = 0,
            ENABLE_2 = 1
        ]
    ],
    SD_CARD_OPTION [
        SOME_TIMEOUT OFFSET(0) NUMBITS(4) [],
        DATA_TIMEOUT OFFSET(4) NUMBITS(4) [],
        BUS_WIDTH OFFSET(15) NUMBITS(1) [
            BIT = 1,
            NYBBLE = 0
        ]
    ],
    SD_ERROR_DETAIL_STATUS0 [
        RCMDE OFFSET(0) NUMBITS(1) [],
        SCMDE OFFSET(1) NUMBITS(1) [],
        CEBER OFFSET(2) NUMBITS(1) [],
        SEBER OFFSET(3) NUMBITS(1) [],
        REBER OFFSET(4) NUMBITS(1) [],
        WEBER OFFSET(5) NUMBITS(1) [],
        CCRCE OFFSET(8) NUMBITS(1) [],
        SCRCE OFFSET(9) NUMBITS(1) [],
        RCRCE OFFSET(10) NUMBITS(1) [],
        WCRCE OFFSET(11) NUMBITS(1) []
    ],
    SD_ERROR_DETAIL_STATUS1 [
        NCR OFFSET(0) NUMBITS(1) [],
        NRS OFFSET(1) NUMBITS(1) [],
        NRCS OFFSET(4) NUMBITS(1) [],
        NWCS OFFSET(5) NUMBITS(1) [],
        KBSY OFFSET(6) NUMBITS(1) []
    ],
    SD_CARD_IRQ_STAT [
        CARDIRQ OFFSET(0) NUMBITS(1) [],
        BLOCKIRQ OFFSET(1) NUMBITS(1) [],
        TXUNDERRUNIRQ OFFSET(2) NUMBITS(1) [],
        DATAENDIRQ OFFSET(14) NUMBITS(1) [],
        RANDOMIRQ OFFSET(15) NUMBITS(1) []
    ],
    SD_DATA_CTL [
        DATA_WIDTH OFFSET(1) NUMBITS(1) [
            HALFWORD = 0,
            WORD = 1
        ]
    ],
    SD_SOFT_RESET [
        SRST OFFSET(0) NUMBITS(1) [
            RESET = 0,
            RELEASE = 1
        ]
    ],
    SD_DATA32_IRQ [
        DATAMODE OFFSET(1) NUMBITS(1) [
            DATA16 = 0,
            DATA32 = 1
        ],
        FIFO32_FULL OFFSET(8) NUMBITS(1) [],
        FIFO32_NOT_EMPTY OFFSET(9) NUMBITS(1) [],
        CLEAR_FIFO32 OFFSET(10) NUMBITS(1) [],
        RX32RDY_EN OFFSET(11) NUMBITS(1) [],
        TX32RQ_EN OFFSET(12) NUMBITS(1) []
    ]
}

#[allow(non_snake_case)]
#[repr(C, packed)]
pub struct RegisterBlock {
    /// SD_CMD - Command and Response Data Type (RW)
    pub SD_CMD: ReadWrite<u16, SD_CMD::Register>, // 0x1000_6000
    pub SD_CARD_PORT_SELECT: ReadWrite<u16, SD_CARD_PORT_SELECT::Register>, // 0x1000_6002
    pub SD_CMD_PARAM0: ReadWrite<u16>,                                      // 0x1000_6004
    pub SD_CMD_PARAM1: ReadWrite<u16>,                                      // 0x1000_6006
    pub SD_STOP_INTERNAL_ACTION: ReadWrite<u16, SD_STOP_INTERNAL_ACTION::Register>, // 0x1000_6008
    pub SD_DATA16_BLK_COUNT: ReadWrite<u16>,                                // 0x1000_600A
    pub SD_RESPONSE0: ReadOnly<u16>,                                        // 0x1000_600C
    pub SD_RESPONSE1: ReadOnly<u16>,                                        // 0x1000_600E
    pub SD_RESPONSE2: ReadOnly<u16>,                                        // 0x1000_6010
    pub SD_RESPONSE3: ReadOnly<u16>,                                        // 0x1000_6012
    pub SD_RESPONSE4: ReadOnly<u16>,                                        // 0x1000_6014
    pub SD_RESPONSE5: ReadOnly<u16>,                                        // 0x1000_6016
    pub SD_RESPONSE6: ReadOnly<u16>,                                        // 0x1000_6018
    pub SD_RESPONSE7: ReadOnly<u16>,                                        // 0x1000_601A
    pub SD_IRQ_STATUS0: ReadWrite<u16, SD_IRQ_STATUS0::Register>,           // 0x1000_601C
    pub SD_IRQ_STATUS1: ReadWrite<u16, SD_IRQ_STATUS1::Register>,           // 0x1000_601E
    pub SD_IRQ_MASK0: ReadWrite<u16, SD_IRQ_STATUS0::Register>,             // 0x1000_6020
    pub SD_IRQ_MASK1: ReadWrite<u16, SD_IRQ_STATUS1::Register>,             // 0x1000_6022
    pub SD_CARD_CLK_CTL: ReadWrite<u16, SD_CARD_CLK_CTL::Register>,         // 0x1000_6024
    pub SD_DATA16_BLK_LEN: ReadWrite<u16>,                                  // 0x1000_6026
    pub SD_CARD_OPTION: ReadWrite<u16, SD_CARD_OPTION::Register>,           // 0x1000_6028
    __unused2: u16,                                                         // 0x1000_602a
    pub SD_ERROR_DETAIL_STATUS0: ReadWrite<u16, SD_ERROR_DETAIL_STATUS0::Register>, // 0x1000_602c
    pub SD_ERROR_DETAIL_STATUS1: ReadWrite<u16, SD_ERROR_DETAIL_STATUS1::Register>, // 0x1000_602e
    pub SD_DATA16_FIFO: ReadWrite<u16>,                                     // 0x1000_6030
    __unused3: u16,                                                         // 0x1000_6032
    pub SD_CARD_IRQ_CTL: ReadWrite<u16>,                                    // 0x1000_6034
    pub SD_CARD_IRQ_STAT: ReadWrite<u16, SD_CARD_IRQ_STAT::Register>,       // 0x1000_6036
    pub SD_CARD_IRQ_MASK: ReadWrite<u16, SD_CARD_IRQ_STAT::Register>,       // 0x1000_6038
    __unused4: [u8; 158],                                                   // 0x1000_603a
    pub SD_DATA_CTL: ReadWrite<u16, SD_DATA_CTL::Register>,                 // 0x1000_60d8
    __unused5: [u8; 6],                                                     // 0x1000_60da
    pub SD_SOFT_RESET: ReadWrite<u16, SD_SOFT_RESET::Register>,             // 0x1000_60e0
    __unused6: [u8; 14],                                                    // 0x1000_60e2
    pub SD_WRPROTECT: ReadOnly<u16>,                                        // 0x1000_60f6
    pub SD_EXT_IRQ_STAT0: ReadWrite<u16>,                                   // 0x1000_60f8
    pub SD_EXT_IRQ_STAT1: ReadWrite<u16>,                                   // 0x1000_60fa
    pub SD_EXT_IRQ_MASK0: ReadWrite<u16>,                                   // 0x1000_60fc
    pub SD_EXT_IRQ_MASK1: ReadWrite<u16>,                                   // 0x1000_60fe
    pub SD_DATA32_IRQ: ReadWrite<u16, SD_DATA32_IRQ::Register>,             // 0x1000_6100
    __unused7: u16,                                                         // 0x1000_6102
    pub SD_DATA32_BLK_LEN: ReadWrite<u16>,                                  // 0x1000_6104
    __unused8: u16,                                                         // 0x1000_6106
    pub SD_DATA32_BLK_CNT: ReadWrite<u16>,                                  // 0x1000_6108
    __unused9: u16,                                                         // 0x1000_610a
    pub SD_DATA32_FIFO: ReadWrite<u32>,                                     // 0x1000_610c
}
